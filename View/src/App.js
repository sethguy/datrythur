import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { HashRouter } from 'react-router-dom'

import Login from './Components/Login.js'

import Main from './Components/Main.js'

import LoadPic from './Components/LoadPic.js'

import ViewShop from './Components/ViewShop.js'

import ViewShops from './Components/ViewShops.js'

import ViewOrders from './Components/ViewOrders.js'

import { Route } from 'react-router-dom'

import createHistory from 'history/createBrowserHistory'

const history = createHistory()

class App extends Component {

  render() {

    return (
      <HashRouter history={ history } basename='/'>
        <div className="App">
          <Route exact path="/" component={ Main } />
          <Route path="/login" component={ Login } />
          <Route path="/LoadPic" component={ LoadPic } />
          <Route path="/ViewShops" component={ ViewShops } />
          <Route path="/ViewShop" component={ ViewShop } />
          <Route path="/ViewOrders" component={ ViewOrders } />
        </div>
      </HashRouter>
      );
  }

}

export default App;